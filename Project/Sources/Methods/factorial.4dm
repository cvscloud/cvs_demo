  //%attributes = {}
  // factorial method
  // TB
  // for Nora; write a factorial function using recursion in 4D

C_LONGINT($i)
Case of 
	:(Count parameters=1)
		For ($i;0;n)
			If (n<=1)
				$0=1
			else
				$0 = n*factorial(n-1)
			End if 
		End for 
end case 