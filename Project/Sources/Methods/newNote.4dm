//%attributes = {}

  // ----------------------------------------------------
  // User name (OS): Barclay Berry
  // Date and time: 05/18/20, 15:07:51
  // ----------------------------------------------------
  // Method: newNote
  // Description
  // 
  //
  // Parameters
  // ----------------------------------------------------
  //test comment

C_OBJECT:C1216($1;$o)

C_LONGINT:C283($0;$iError)

$o:=New object:C1471

If (Count parameters:C259=1)
	$o:=$1
Else 
	$o.length:=0
End if 

If ($o.length>0)
	  //test new method by IBB
	CREATE RECORD:C68([Notes:2])
	[Notes:2]customerID:2:=$o.customerID
	[Notes:2]theSubject:5:=$o.subject
	[Notes:2]theNote:6:=$o.note
	[Notes:2]createDate:3:=Current date:C33
	[Notes:2]createTime:4:=Current time:C178
	SAVE RECORD:C53([Notes:2])
	
Else 
	$iError:=-1
End if 


$iError:=0




$0:=$iError