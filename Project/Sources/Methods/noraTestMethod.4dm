//Nora's test method 
//changed line 10


C_TEXT:C284($1;$customerName)
C_OBJECT:C1216($customerObj)

Case of 
	: (Count parameters:C259=0)
		$customerName:="Bank of Jamaica"  // Good for testing #TB
		
	: (Count parameters:C259=1)
		$customerName:=$1
	Else 
		ASSERT:C1129(False:C215;"Invalid no of parameters")
End case 

ASSERT:C1129(True:C214)
$customerObj:=New object:C1471("customer_name";$customerName)/* Create an object */
$0:=$customerObj  // return the object
