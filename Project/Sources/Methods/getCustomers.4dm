//%attributes = {}
  // getCustomers: 
  // No parameters
  // Created by: Jaime Alvarez, April 28/2020
  // Modified By Tiran; minor changes
  // New Comment 1 By Jaime
  // New Comment 2 By Jaime
  // New Comment 3 By Jaime
  //New Comment 4 by Blake

C_TEXT:C284($1;$customerID)
C_OBJECT:C1216($0;$customers)

Case of 
	: (Count parameters:C259=0)
		
		  // Use ORDA to get All the Customers
		$customers:=ds:C1482.Customers.all()
		
	: (Count parameters:C259=1)
		
		  // Use ORDA to get the Customer
		$customerID:=$1
		$customers:=ds:C1482.Customers.query("customerID = :1";$customerID)
		
	Else 
		ASSERT:C1129(False:C215;"Invalid number of parameters in "+Current method name:C684)
End case 

  // Return customers
  //Adding conflicting Line
$0:=$customers
