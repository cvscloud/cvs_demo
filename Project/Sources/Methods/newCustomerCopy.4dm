//%attributes = {}
  //Amir 4th May 2020 //THIS IS ADDED BY AMIR
  //returns a customer object for given customer name
  //<C_OBJECT>:=newCustomer(customer_name <C_TEXT>)
  // this is my change it may not be a good idea! ... I like to play around
  // tested! and I like it

C_TEXT:C284($1;$customerName)
C_OBJECT:C1216($customerObj)

Case of 
	: (Count parameters:C259=0)
		$customerName:="Lotus FX"  // Good for testing #TB
		
	: (Count parameters:C259=1)
		$customerName:=$1
	Else 
		ASSERT:C1129(False:C215;"Invalid no of parameters")
End case 

ASSERT:C1129(True:C214)
$customerObj:=New object:C1471("customer_name";$customerName)/* Create an object */
$0:=$customerObj  // return the object
